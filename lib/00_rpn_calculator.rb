require 'pry'

class RPNCalculator
  
  NUM_STR = [*"0".."9"]
  ASSOCIATIVE = [:-, :/]
  COMMUTATIVE = [:+, :*]
  ALL_OPS = COMMUTATIVE + ASSOCIATIVE
  
  def initialize
    @stack = []
  end
  
  def push(n)
    @stack << n
  end
  
  def plus
    raise StandardError, "calculator is empty" if @stack.empty?
    @stack << :+
  end
  
  def minus
    raise StandardError, "calculator is empty" if @stack.empty?
    @stack << :-
  end
  
  def divide
    raise StandardError, "calculator is empty" if @stack.empty?
    @stack << :/
  end
  
  def times
    raise StandardError, "calculator is empty" if @stack.empty?
    @stack << :*
  end
  
  def tokens(str)
    str.split.map { |x| NUM_STR.include?(x) ? x.to_i : x.to_sym }
  end    
  
  def evaluate(str)
    stack = tokens(str)
    rpn_eval_stack(stack)[-1]
  end
    
  def value
    if res = rpn_eval_stack(@stack.dup)
      @stack = res
      return @stack[-1]
    else
      raise StandardError, "calculator is empty" 
    end
  end
  
  private
  
  def rpn_eval_stack(stack)
    stack_evaluated = false
    while !stack_evaluated
      # iterate from the top of the stack.
      -1.downto(stack.length * -1).each do |i|
        arg1, arg2, op = stack[i-2], stack[i-1], stack[i]
        # if evaluatable, update stack and break from loop.
        if res = rpn_eval(op, arg1, arg2)
          stack[i-2..i] = [res]
          break
        end
        # reached bottom of stack and nothing can be evaluated further.
        stack_evaluated = true if i == stack.length * -1
      end
    end
    stack[-1].is_a?(Numeric) ? stack : nil
  end
  
  # returns an number value or nil if arguments cant be evaluated.
  def rpn_eval(op, n, m)
    return nil if !ALL_OPS.include?(op) || !n.is_a?(Numeric) || !m.is_a?(Numeric)
    res = nil
    if COMMUTATIVE.include?(op) || op == :-
      res = n.send(op, m) 
    elsif op == :/
      res = n.to_f.send(op, m)
    end
    res
  end
  
end
